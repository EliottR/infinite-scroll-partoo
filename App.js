import React, { useEffect, useState } from "react";
import {
  ActivityIndicator,
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from "react-native";

export default function App() {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [endScroll, setEndScroll] = useState(false);

  const fetchData = () => {
    setIsLoading((prev) => (prev = true));

    setTimeout(() => {
      setData((prev) => [
        ...prev,
        { name: "Brazil" },
        { name: "Nigeria" },
        { name: "Bangladesh" },
      ]);
      setIsLoading((prev) => (prev = false));
    }, 1000);
  };
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        style={styles.list}
        onEndReachedThreshold={0.1}
        onEndReached={() => {
          setEndScroll(true);
        }}
        onMomentumScrollEnd={() => {
          endScroll && fetchData();
          setEndScroll(false);
        }}
        data={data}
        renderItem={({ item }) => (
          <View style={styles.listItem}>
            <Text>{item.name}</Text>
          </View>
        )}
      />
      {isLoading && <ActivityIndicator />}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  list: {
    width: "100%",
    height: "100%",
  },
  listItem: {
    width: "100%",
    height: 40,
    padding: 8,
    alignItems: "flexStart",
  },
});
